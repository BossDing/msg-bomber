# encoding=utf8
import os
import json


# 扫描目录中的配置文件
def scan_dir_to_json(path, ext):
    if not os.path.isdir(path):
        print "{0} is not directory...".format(path)
        exit(-1)
    content_array = []
    for fileName in os.listdir(path):
        if fileName.endswith(ext):
            f = open(path + '/' + fileName, 'r')
            content = f.read()
            content_array.append(json.loads(content))
        else:
            print "{0} is not json file...".format(fileName)
    return content_array

